const register = [];
// const loggedIn = false;

function registerUser(email, password, confirmPass) {
  register.push({
    email,
    password,
    confirmPass,
  });
}

function loginUser(email, password) {
  let test = false;

  register.forEach((user) => {
    if (user.email === email && user.password === password) {
      // loggedIn = true;
      test = true;
    } else {
      // loggedIn = false;
      test = false;
    }
  });
  return test;
}

// export { registerUser, loginUser, loggedIn };
export { registerUser, loginUser };
