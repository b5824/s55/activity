import { Navbar, Container, Nav } from 'react-bootstrap';
import { useState, useContext } from 'react';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';
import { Navigate } from 'react-router-dom';

export default function AppNavBar() {
  // const [user, setUser] = useState(localStorage.getItem('email'));
  // console.log(user);

  const { user } = useContext(UserContext);
  // console.log(user);

  return (
    <Navbar bg='light' expand='lg'>
      <Container>
        <Navbar.Brand as={Link} to={'/'}>
          Booking app
        </Navbar.Brand>
        <Navbar.Toggle aria-controls='basic-navbar-nav' />
        <Navbar.Collapse id='basic-navbar-nav'>
          <Nav className='me-auto'>
            <Nav.Link as={Link} to='/'>
              Home
            </Nav.Link>
            <Nav.Link as={Link} to='/courses'>
              Courses
            </Nav.Link>

            {user.id !== null ? (
              <>
                <Nav.Link as={Link} to='/logout'>
                  Logout
                </Nav.Link>
                {/* <Navigate to='/courses' /> */}
              </>
            ) : (
              <>
                <Nav.Link as={Link} to='/register'>
                  Register
                </Nav.Link>
                <Nav.Link as={Link} to='/login'>
                  Login
                </Nav.Link>
              </>
            )}
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}
