import { Row, Col, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Banner(props) {
  console.log(props);
  return (
    <Row>
      <Col className='p-5'>
        {props.status === '200' ? (
          <>
            <h1>Booking App - 182</h1>
            <p>Enroll courses here</p>
            <Button variant='primary' as={Link} to={'/courses'}>
              Enroll Now!
            </Button>
          </>
        ) : (
          <>
            <h1 className='font-monospace text-center'>{props.status}</h1>
            <h3 className='font-monospace text-center'>
              {props.status !== '403'
                ? 'Page Not Found'
                : "Forbidden. You're currently logged in."}
            </h3>
            <div className='text-center'>
              <Button
                variant='outline-danger'
                as={Link}
                to={'/'}
                className='mt-3 font-monospace'
              >
                Back to homepage
              </Button>
            </div>
          </>
        )}
      </Col>
    </Row>
  );
}
