import AppNavBar from './components/AppNavBar';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import './App.css';
import Home from './Pages/Home';
import Courses from './Pages/Courses';
import Register from './Pages/Register';
import Login from './Pages/Login';
import Logout from './Pages/Logout';
import Error from './Pages/Error';
import { useEffect, useState } from 'react';
import { UserProvider } from './UserContext';
import CourseView from './Pages/CourseView';

function App() {
  //usestate is used for when you're storing information and if there's any changes to the stored information
  const [user, setUser] = useState({
    id: null,
    isAdmin: null,
  });

  console.log(user);

  const unsetUser = () => {
    localStorage.clear();
  };

  useEffect(() => {
    // console.log(user); //result: _id and isAdmin
    // console.log(localStorage); //result: accessToken

    fetch('http://localhost:4000/users/getUserDetails', {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log('DATA ----- ', data);
        //set the user states values with the suer details upon successful login
        if (typeof data._id !== 'undefined') {
          setUser({
            id: data._id,
            isAdmin: data.isAdmin,
          });
        } else {
          //set back the initial state of user
          setUser({
            id: null,
            isAdmin: null,
          });
        }
      });
  }, []);

  return (
    <UserProvider value={{ user, setUser, unsetUser }}>
      <Router>
        <AppNavBar />
        <Container>
          <Routes>
            <Route exac path='/' element={<Home />} />
            <Route exac path='/courses' element={<Courses />} />
            <Route exac path='/courseView/:courseId' element={<CourseView />} />
            <Route exac path='/register' element={<Register />} />
            <Route exac path='/login' element={<Login />} />
            <Route exac path='/logout' element={<Logout />} />
            <Route path='*' element={<Error />} />
          </Routes>
        </Container>
      </Router>
    </UserProvider>
  );
}

export default App;

/* 
  Reading List:
	>>https://reactrouter.com/docs/en/v6/getting-started/overview
	>>https://upmostly.com/tutorials/react-filter-filtering-arrays-in-react-with-examples
	>>https://bobbyhadz.com/blog/react-export-multiple-functions#:~:text=Use%20named%20exports%20to%20export,necessary%20in%20a%20single%20file.
	>>https://reactjs.org/docs/lists-and-keys.html#keys
	>>https://stackoverflow.com/questions/32128978/react-router-no-not-found-route
	>>https://stackoverflow.com/questions/69868956/how-to-redirect-in-react-router-v6
	>>https://stackoverflow.com/questions/45089386/what-is-the-best-way-to-redirect-a-page-using-react-router
*/
