import { useEffect, useState } from 'react';
import CourseCard from '../components/CourseCard';
// import coursesData from '../data/coursesData';

export default function Courses() {
  //   console.log(coursesData);
  //   console.log(coursesData[0]);

  // const courses = coursesData.map((course) => {
  //   //   key - identidies the elements in the map method. Each one of them needs to have their own key. It's similar to an ID. If there's no key defined, the courses is not stable because if one of them change, it won't be able to identify which one to change.
  //   return <CourseCard key={course.id} courseProp={course} />;
  // });
  // console.log(courses);

  const [courses, setCourses] = useState([]);

  useEffect(() => {
    fetch('http://localhost:4000/courses')
      .then((result) => result.json())
      .then((data) => {
        setCourses(
          data.map((course) => {
            return <CourseCard key={course._id} courseProp={course} />;
          })
        );
      });
  }, []);

  return (
    <>
      <h1>Courses Available:</h1>
      {/* Props is used to pass data from one file to another */}
      {/* <CourseCard courseProp={coursesData} /> */}

      {courses}
    </>
  );
}
