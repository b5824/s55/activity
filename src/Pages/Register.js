// /*
// NOTE:    How Does React Handle Forms?
// */
// import { Form, Button } from 'react-bootstrap';
// import { useEffect, useState } from 'react';

// export default function Register() {
//   const [email, setEmail] = useState('');
//   const [password, setPassword] = useState('');
//   const [confirmPass, setConfirmPass] = useState('');
//   const [isActive, setIsActive] = useState(false);

//   // console.log(email);
//   // console.log(password);
//   // console.log(confirmPass);

//   useEffect(() => {
//     if (
//       email !== '' &&
//       password !== '' &&
//       confirmPass !== '' &&
//       password === confirmPass
//     ) {
//       setIsActive(true);
//     } else {
//       setIsActive(false);
//     }
//   }, [email, password, confirmPass]);

//   const registerUser = (e) => {
//     e.preventDefault();
//     setEmail('');
//     setPassword('');
//     setConfirmPass('');
//   };

//   return (
//     <>
//       <h1>Register Form:</h1>
//       {/* You ALWAYS have to access the event first before calling the function and passing in the event. BEAR IN MIND that you need to first identify where the attributes belongs to and then attach it to its parent. It needs to placed exactly where it is. */}
//       <Form onSubmit={(e) => registerUser(e)}>
//         <Form.Group controlId='userEmail'>
//           <Form.Label>Email address:</Form.Label>
//           <Form.Control
//             type='email'
//             placeholder='Enter your email'
//             required
//             value={email}
//             onChange={(e) => setEmail(e.target.value)}
//           />
//           <Form.Text className='text-muted'>
//             We'll never share your email with anyone.
//           </Form.Text>
//         </Form.Group>

//         <Form.Group controlId='password'>
//           <Form.Label>Password:</Form.Label>
//           <Form.Control
//             type='password'
//             placeholder='Enter your password'
//             required
//             value={password}
//             onChange={(e) => setPassword(e.target.value)}
//           />
//         </Form.Group>

//         <Form.Group controlId='password2'>
//           <Form.Label>Verify Password:</Form.Label>
//           <Form.Control
//             type='password'
//             placeholder='Confirm password'
//             required
//             value={confirmPass}
//             onChange={(e) => setConfirmPass(e.target.value)}
//           />
//         </Form.Group>

//         {/*       {isActive ? (
//           <Button
//             variant='success'
//             type='submit'
//             id='submitBtn'
//             className='mt-3 mb-3'
//           >
//             Submit
//           </Button>
//         ) : (
//           <Button
//             variant='danger'
//             type='submit'
//             id='submitBtn'
//             className='mt-3 mb-3'
//             disabled
//           >
//             Submit
//           </Button>
//         )} */}

//         <Button
//           variant={isActive ? 'success' : 'danger'}
//           type='submit'
//           id='submitBtn'
//           className='mt-3 mb-3'
//           disabled={isActive ? false : true}
//         >
//           Submit
//         </Button>
//       </Form>
//     </>
//   );
// }

import { Form, Button } from 'react-bootstrap';
import { useEffect, useState, useContext } from 'react';
import { registerUser } from '../data/user';
import { Navigate, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
/* 
NOTE:    How Does React Handle Forms?
*/

export default function Register() {
  const { user } = useContext(UserContext);
  console.log(user); //result: {email:null}

  const history = useNavigate();

  const [firstName, setFirstName] = useState('');
  const [lastName, setlastName] = useState('');
  const [mobileNo, setMobileNo] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  // const [confirmPass, setConfirmPass] = useState('');
  const [isActive, setIsActive] = useState(false);

  useEffect(() => {
    if (
      firstName !== '' &&
      lastName !== '' &&
      mobileNo !== '' &&
      email !== '' &&
      password !== ''
    ) {
      registerUser(email, password);
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [firstName, lastName, mobileNo, email, password]);

  const registerUsers = (e) => {
    e.preventDefault();

    fetch('http://localhost:4000/users/checkEmailExists', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email: email,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          registerUserDB();

          // Swal.fire({
          //   title: 'Successfully logged in',
          //   icon: 'success',
          //   text: 'Enroll in our courses',
          // });
          // } else {
          //   Swal.fire({
          //     title: 'Duplicate email',
          //     icon: 'error',
          //     text: 'Please enter a new email.',
          //   });
        }
      });

    setEmail('');
    setPassword('');
    setFirstName('');
    setlastName('');
    setMobileNo('');
  };

  const registerUserDB = () => {
    fetch('http://localhost:4000/users/', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        firstName: firstName,
        lastName: lastName,
        mobileNo: mobileNo,
        email: email,
        password: password,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data.email) {
          Swal.fire({
            title: 'Successfully registered',
            icon: 'success',
            text: 'Enroll in our courses',
          });
          history('/login');
        } else {
          Swal.fire({
            title: 'Registration failed',
            icon: 'error',
            text: 'Something went wrong',
          });
        }
      });
  };
  console.log(user);
  return user.id === null ? (
    <>
      <h1>Register Form:</h1>
      {/* You ALWAYS have to access the event first before calling the function and passing in the event. BEAR IN MIND that you need to first identify where the attributes belongs to and then attach it to its parent. It needs to placed exactly where it is. */}
      <Form onSubmit={(e) => registerUsers(e)}>
        <Form.Group controlId='firstName'>
          <Form.Label>First Name:</Form.Label>
          <Form.Control
            type='text'
            placeholder='Please input your first name here'
            required
            value={firstName}
            onChange={(e) => setFirstName(e.target.value)}
          />
        </Form.Group>

        <Form.Group controlId='lastName'>
          <Form.Label>Last Name:</Form.Label>
          <Form.Control
            type='text'
            placeholder='Please input your first name here'
            required
            value={lastName}
            onChange={(e) => setlastName(e.target.value)}
          />
        </Form.Group>

        <Form.Group controlId='mobileNo'>
          <Form.Label>Mobile Number:</Form.Label>
          <Form.Control
            type='text'
            placeholder='Please input your mobile here'
            required
            value={mobileNo}
            onChange={(e) => setMobileNo(e.target.value)}
          />
        </Form.Group>

        <Form.Group controlId='userEmail'>
          <Form.Label>Email address:</Form.Label>
          <Form.Control
            type='email'
            placeholder='Enter your email'
            required
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
          <Form.Text className='text-muted'>
            We'll never share your email with anyone.
          </Form.Text>
        </Form.Group>

        <Form.Group controlId='password'>
          <Form.Label>Password:</Form.Label>
          <Form.Control
            type='password'
            placeholder='Enter your password'
            required
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </Form.Group>

        {/*       {isActive ? (
          <Button
            variant='success'
            type='submit'
            id='submitBtn'
            className='mt-3 mb-3'
          >
            Submit
          </Button>
        ) : (
          <Button
            variant='danger'
            type='submit'
            id='submitBtn'
            className='mt-3 mb-3'
            disabled
          >
            Submit
          </Button>
        )} */}

        <Button
          variant={isActive ? 'success' : 'danger'}
          type='submit'
          id='submitBtn'
          className='mt-3 mb-3'
          disabled={isActive ? false : true}
          element={<Navigate replace to='/login' />}
        >
          Submit
        </Button>
      </Form>
    </>
  ) : (
    <>
      <Navigate to='/courses' />
    </>
  );
}
