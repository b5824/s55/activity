import { useEffect, useState, useContext } from 'react';
import { Card, Container, Row, Col, Button } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function CourseView() {
  const { user } = useContext(UserContext);
  const history = useNavigate();
  //Retrieves the courseId passed in the URL
  const { courseId } = useParams();

  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState(0);

  const enroll = (courseId) => {
    fetch('http://localhost:4000/users/enroll', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify({ courseId: courseId }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          Swal.fire({
            title: 'Successfully Enrolled',
            icon: 'success',
            text: 'Thank you for enrolling',
          });

          history('/courses');
        } else {
          Swal.fire({
            title: 'Something went wrong',
            icon: 'error',
            text: 'Please try again later.',
          });
        }
      });
  };

  useEffect(() => {
    console.log(courseId);
    fetch(`http://localhost:4000/courses/getSingleCourse/${courseId}`)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setName(data.name);
        setDescription(data.description);
        setPrice(data.price);
      });
  }, [courseId]);

  return (
    <Container className='mt-5'>
      <Row>
        <Col lg={{ span: 6, offset: 3 }}>
          <Card>
            <Card.Body>
              <Card.Title>{name}</Card.Title>
              <Card.Subtitle>Description:</Card.Subtitle>
              <Card.Text>{description}</Card.Text>
              <Card.Subtitle>Price:</Card.Subtitle>
              <Card.Text>{price}</Card.Text>
              <Card.Subtitle>Class Schedule</Card.Subtitle>
              <Card.Text>8:00-17:00</Card.Text>
              {user.id !== null ? (
                <Button variant='primary' onClick={() => enroll(courseId)}>
                  Enroll
                </Button>
              ) : (
                <Link className='btn btn-danger' to='/login'>
                  Login
                </Link>
              )}
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}
