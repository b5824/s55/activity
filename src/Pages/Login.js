/*  Activity s52
>> Create Login.js under pages folder
>> The user should input their email and password for logging in
>> When all the input fields are filled, enable the submit button with a different variant color.
    >> The login button should be disabled and has a different variant color before all fields are filled.
>> Upon clicking the submit button, a message will alert the user of a successful login.
>> After clicking the alert, login field should be cleared and will not redirect to any part of the page
>> Render the login page in App.js
>> Send your outputs in Hangouts and link it in Boodle
*/
import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import { Link, Navigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Login(props) {
  const [isActive, setIsActive] = useState(false);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isLoggedIn, setLoggedIn] = useState(false);

  // console.log(isLoggedIn);

  const { user, setUser } = useContext(UserContext);

  const checker = (e) => {
    e.preventDefault();

    //NOTE: Fetching data
    fetch('http://localhost:4000/users/login', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        // setLoggedIn(true);
        // console.log(isLoggedIn);

        if (typeof data.accessToken !== 'undefined') {
          localStorage.setItem('token', data.accessToken);
          setLoggedIn(true);
          retrieveUserDetails(data.accessToken);

          //NOTE: Swal - alert

          Swal.fire({
            title: 'Login Successful',
            icon: 'success',
            text: 'Welcome to Booking App of 182',
          });
        } else {
          setLoggedIn(false);
          Swal.fire({
            title: 'Authentication failed',
            icon: 'error',
            text: 'Check your credentials',
          });
        }
      });

    // localStorage.setItem('email', email);

    // setUser({
    //   email: localStorage.getItem('email'),
    // });

    // let test = loginUser(email, password);

    // isActive ? alert('Logged in') : alert('User not found');
    setEmail('');
    setPassword('');
    // console.log(isLoggedIn);
    // isLoggedIn
    //   ? Swal.fire({
    //       title: 'Login Successful',
    //       icon: 'success',
    //       text: 'Welcome to Booking App of 182',
    //     })
    //   : Swal.fire({
    //       title: 'Authentication failed',
    //       icon: 'error',
    //       text: 'Check your credentials',
    //     });
  };

  const retrieveUserDetails = (token) => {
    fetch('http://localhost:4000/users/getUserDetails', {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setUser({
          _id: data._id,
          isAdmin: data.isAdmin,
        });
      });
  };

  /* 
    {
    "_id": "62970180054bdc27aab1d8f8",
    "firstName": "Lester",
    "lastName": "Alarcon",
    "email": "lessy@test.com",
    "password": "$2a$10$H45HQ1hlzKKGrj.xoAodz.ykBKo8xIvsMNZYLZBVyvJ//GJIn/9nK",
    "mobileNo": "012312534",
    "isAdmin": false,
    "enrollments": 
    }

    objName._id
    {
      email: "Value"
    }
  */

  //you tell React that your component needs to do something after render.
  //You can tell React to skip applying an effect if certain values haven’t changed between re-renders. To do so, pass an array as an optional second argument to useEffect:
  useEffect(() => {
    if (email !== '' && password !== '') {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password]);
  // console.log('USER --- ', user);
  return user.id !== null ? (
    <Navigate to='/courses' />
  ) : (
    <>
      <h1>Login</h1>
      <Form onSubmit={(e) => checker(e)}>
        <Form.Group className='mb-3' controlId='formBasicEmail'>
          <Form.Label>Email address</Form.Label>
          <Form.Control
            type='email'
            placeholder='Enter email'
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            required
          />
          <Form.Text className='text-muted'>
            We'll never share your email with anyone else.
          </Form.Text>
        </Form.Group>

        {/* https://upmostly.com/tutorials/react-onchange-events-with-examples */}
        <Form.Group className='mb-3' controlId='formBasicPassword'>
          <Form.Label>Password</Form.Label>
          <Form.Control
            type='password'
            placeholder='Password'
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            required
          />
        </Form.Group>
        <p>
          Not yet registered? <Link to='/register'>Register Here</Link>
        </p>
        <Button
          variant={isActive ? 'success' : 'danger'}
          type='submit'
          id='submitBtn'
          className='mt-3 mb-3'
          disabled={isActive ? false : true}
        >
          Submit
        </Button>
      </Form>
    </>
  );
}

/* 
  Reading Lists:
    >>https://sweetalert2.github.io/

*/
