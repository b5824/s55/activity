import { Navigate } from 'react-router-dom';
import { useContext, useEffect } from 'react';
import UserContext from '../UserContext';

export default function Logout() {
  //   localStorage.clear();

  const { setUser, unsetUser } = useContext(UserContext);

  unsetUser();

  useEffect(() => {
    setUser({ id: null });
  });
  //Navigate is redirection
  return <Navigate to='/login' />;
}
